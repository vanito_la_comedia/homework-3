package config

import (
	"gopkg.in/yaml.v3"
)

type Pvz struct {
	Host       string   `yaml:"host"`
	Port       int      `yaml:"port"`
	Brokers    []string `yaml:"brokers"`
	Dbhost     string   `yaml:"dbhost"`
	Dbport     int      `yaml:"dbport"`
	Dbuser     string   `yaml:"dbuser"`
	Dbpassword string   `yaml:"dbpassword"`
	Dbname     string   `yaml:"dbname"`
	DbSslmode  string   `yaml:"dbsslmode"`
}

type Payment struct {
	Host       string   `yaml:"host"`
	Port       int      `yaml:"port"`
	Brokers    []string `yaml:"brokers"`
	Dbhost     string   `yaml:"dbhost"`
	Dbport     int      `yaml:"dbport"`
	Dbuser     string   `yaml:"dbuser"`
	Dbpassword string   `yaml:"dbpassword"`
	Dbname     string   `yaml:"dbname"`
	DbSslmode  string   `yaml:"dbsslmode"`
}

type Monitoring struct {
	Host       string   `yaml:"host"`
	Port       int      `yaml:"port"`
	Gwhost     string   `yaml:"gwhost"`
	Gwport     int      `yaml:"gwport"`
	Brokers    []string `yaml:"brokers"`
	Dbhost     string   `yaml:"dbhost"`
	Dbport     int      `yaml:"dbport"`
	Dbuser     string   `yaml:"dbuser"`
	Dbpassword string   `yaml:"dbpassword"`
	Dbname     string   `yaml:"dbname"`
	DbSslmode  string   `yaml:"dbsslmode"`
}

type Config struct {
	Pvz        Pvz
	Payment    Payment
	Monitoring Monitoring
}

func ParseConfig(fileBytes []byte) (*Config, error) {
	cf := Config{}

	err := yaml.Unmarshal(fileBytes, &cf)
	if err != nil {
		return nil, err
	}
	return &cf, nil
}

type Order struct {
	ID    int    `json:"id"`
	State string `json:"state"`
}
