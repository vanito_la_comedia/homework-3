CREATE TABLE IF NOT EXISTS orders_2 (
    id          integer          PRIMARY KEY,
    state       text     NOT NULL
);

CREATE TABLE IF NOT EXISTS payments_2 (
    id          integer          PRIMARY KEY,
    state       text     NOT NULL,
    price       integer  NOT NULL
);

CREATE TABLE IF NOT EXISTS monitoring_2 (
    id          integer  PRIMARY KEY,
    state       text     NOT NULL,
    price       integer  NOT NULL,
    clientid    integer  NOT NULL,
    pvzid       integer  NOT NULL
);

