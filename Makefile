dc_up:
	docker-compose build
	docker-compose up

dc_down:
	docker-compose down
	sudo rm -rf kafka_data
	sudo rm -rf grafana
	docker image prune