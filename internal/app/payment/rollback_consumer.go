package payment

import (
	"context"
	"encoding/json"
	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/vanito_la_comedia/homework-3/config"
	repo "gitlab.ozon.dev/vanito_la_comedia/homework-3/internal/repository/payment"
	"log"
	"time"
)

type RollbackHandler struct {
	P sarama.SyncProducer
	R *payRepo
}

func initRollbackConsumer(ctx context.Context, cfg *config.Config, scfg *sarama.Config, repository *payRepo) (*RollbackHandler, error) {
	producer, err := sarama.NewSyncProducer(cfg.Payment.Brokers, scfg)
	if err != nil {
		return nil, err
	}
	cons, err := sarama.NewConsumerGroup(cfg.Payment.Brokers, "payRollback", scfg)
	if err != nil {
		return nil, err
	}

	rolHandler := &RollbackHandler{
		P: producer,
		R: repository,
	}
	go func() {
		for {
			err := cons.Consume(ctx, []string{"rollback_orders"}, rolHandler)
			if err != nil {
				log.Printf("rollback consumer error: %v", err)
				time.Sleep(time.Second * 5)
			}
		}
	}()

	return rolHandler, nil
}

func (ret *RollbackHandler) Setup(sarama.ConsumerGroupSession) error {
	return nil
}

func (ret *RollbackHandler) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

func (ret *RollbackHandler) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for msg := range claim.Messages() {
		var co config.Order
		err := json.Unmarshal(msg.Value, &co)
		if err != nil {
			log.Print("rollback handler data %v: %v", string(msg.Value), err)
			continue
		}
		ctx := context.Background()
		p := repo.Payment{
			ID:    co.ID,
			State: co.State,
		}
		err = ret.R.Repo.PayPayment(ctx, p)
		if err != nil {
			log.Print("rollback handler can't rollback order %v: %v", string(msg.Value), err)
			continue
		}

		log.Printf("Order %v rollbacked", co.ID)
	}
	return nil
}
