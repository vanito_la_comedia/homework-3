package payment

import (
	"context"
	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/vanito_la_comedia/homework-3/config"
	repo "gitlab.ozon.dev/vanito_la_comedia/homework-3/internal/repository/payment"
	"log"
)

type payment struct {
	returnConsumer   *ReturnHandler
	rollbackConsumer *RollbackHandler
}

type payRepo struct {
	Repo repo.PayRepository
}

func NewRepo(Repo repo.PayRepository) *payRepo {
	return &payRepo{Repo: Repo}
}

func New(ctx context.Context, cfg *config.Config, repository *payRepo) (*payment, error) {

	scfg := sarama.NewConfig()
	scfg.Producer.Return.Successes = true

	retConsumer, err := initReturnConsumer(ctx, cfg, scfg, repository)
	if err != nil {
		log.Printf("failed init return consumer :%v", err)
		return nil, err
	}

	rolConsumer, err := initRollbackConsumer(ctx, cfg, scfg, repository)
	if err != nil {
		log.Printf("failed init rollback consumer :%v", err)
		return nil, err
	}

	return &payment{
		returnConsumer:   retConsumer,
		rollbackConsumer: rolConsumer,
	}, nil
}
