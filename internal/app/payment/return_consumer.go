package payment

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/vanito_la_comedia/homework-3/config"
	repo "gitlab.ozon.dev/vanito_la_comedia/homework-3/internal/repository/payment"
	"log"
	"time"
)

type ReturnHandler struct {
	P sarama.SyncProducer
	R *payRepo
}

func initReturnConsumer(ctx context.Context, cfg *config.Config, scfg *sarama.Config, repository *payRepo) (*ReturnHandler, error) {
	producer, err := sarama.NewSyncProducer(cfg.Payment.Brokers, scfg)
	if err != nil {
		return nil, err
	}
	cons, err := sarama.NewConsumerGroup(cfg.Payment.Brokers, "payment", scfg)
	if err != nil {
		return nil, err
	}

	retHandler := &ReturnHandler{
		P: producer,
		R: repository,
	}
	go func() {
		for {
			err := cons.Consume(ctx, []string{"return_payments"}, retHandler)
			if err != nil {
				log.Printf("return consumer error: %v", err)
				time.Sleep(time.Second * 5)
			}
		}
	}()

	return retHandler, nil
}

func (ret *ReturnHandler) Setup(sarama.ConsumerGroupSession) error {
	return nil
}

func (ret *ReturnHandler) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

func (ret *ReturnHandler) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for msg := range claim.Messages() {
		var co config.Order
		err := json.Unmarshal(msg.Value, &co)
		if err != nil {
			log.Print("return handler data %v: %v", string(msg.Value), err)
			continue
		}
		ctx := context.Background()
		p := repo.Payment{
			ID:    co.ID,
			State: co.State,
		}
		err = ret.R.Repo.ReturnPayment(ctx, p)
		if err != nil {
			log.Print("return handler can't return order %v: %v", string(msg.Value), err)
			continue
		}

		err = ret.ReturnRequest(co)
		if err != nil {
			err := ret.RollbackRequest(co)
			if err != nil {
				log.Printf("Cant send RollbackRequest: %v", err)
			}
			continue
		}
		log.Printf("Payment %v returned", co.ID)
	}
	return nil
}

func (ret *ReturnHandler) ReturnRequest(order config.Order) error {
	co := order
	co.State = "Заказ отменен"
	b, err := json.Marshal(co)
	if err != nil {
		log.Printf("json marshal error: %v", err)
	}

	if co.ID == 1005 {
		return fmt.Errorf("order id 1005 payment error")
	}

	_, _, err = ret.P.SendMessage(&sarama.ProducerMessage{
		Topic: "return_results",
		Key:   sarama.StringEncoder(fmt.Sprintf("%v", co.ID)),
		Value: sarama.ByteEncoder(b),
	})
	if err != nil {
		return fmt.Errorf("Cant send return request: %v", err)
	}
	return nil
}

func (ret *ReturnHandler) RollbackRequest(order config.Order) error {
	b, err := json.Marshal(order)
	if err != nil {
		log.Printf("json marshal error: %v", err)
	}
	_, _, err = ret.P.SendMessage(&sarama.ProducerMessage{
		Topic: "rollback_orders",
		Key:   sarama.StringEncoder(fmt.Sprintf("%v", order.ID)),
		Value: sarama.ByteEncoder(b),
	})
	if err != nil {
		return fmt.Errorf("Cant send rollback request: %v", err)
	}
	return nil
}
