package monitoring

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/Shopify/sarama"
	"github.com/go-redis/redis/v8"
	"gitlab.ozon.dev/vanito_la_comedia/homework-3/config"
	repo "gitlab.ozon.dev/vanito_la_comedia/homework-3/internal/repository/monitoring"
	pb "gitlab.ozon.dev/vanito_la_comedia/homework-3/pkg/api"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"log"
	"time"
)

type ReturnHandler struct {
	R *monRepo
	pb.UnimplementedMonitoringServer
}

type MonOrder struct {
	ID       int    `json:"id"`
	State    string `json:"state"`
	Price    int    `json:"price"`
	ClientID int    `json:"clientid"`
	PvzID    int    `json:"pvzid"`
}

func initReturnConsumer(ctx context.Context, cfg *config.Config, scfg *sarama.Config, repository *monRepo) (*ReturnHandler, error) {
	cons, err := sarama.NewConsumerGroup(cfg.Monitoring.Brokers, "monitoring", scfg)
	if err != nil {
		return nil, err
	}

	retHandler := &ReturnHandler{
		R: repository,
	}
	go func() {
		for {
			err := cons.Consume(ctx, []string{"return_results"}, retHandler)
			if err != nil {
				log.Printf("return consumer error: %v", err)
				time.Sleep(time.Second * 5)
			}
		}
	}()

	return retHandler, nil
}

func (ret *ReturnHandler) Setup(sarama.ConsumerGroupSession) error {
	return nil
}

func (ret *ReturnHandler) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

func (ret *ReturnHandler) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for msg := range claim.Messages() {
		var co config.Order
		err := json.Unmarshal(msg.Value, &co)
		if err != nil {
			log.Print("return handler data %v: %v", string(msg.Value), err)
			continue
		}
		ctx := context.Background()
		o := repo.Monitoring{
			ID:    co.ID,
			State: "Заказ отменен",
		}
		err = ret.R.Repo.UpdateOrder(ctx, o)
		if err != nil {
			log.Print("return handler can't return order %v: %v", string(msg.Value), err)
			continue
		}

		log.Printf("Monitoring order %v returned", co.ID)
	}
	return nil
}

func (ret *ReturnHandler) GetOrderState(ctx context.Context, in *pb.OrderReq) (*pb.Order, error) {

	mo, err := ret.GetMonOrder(ctx, int(in.ID))

	if err != nil {
		return nil, fmt.Errorf("can't get order state: %v", err)
	}

	return &pb.Order{
		ID:       int64(mo.ID),
		State:    mo.State,
		Price:    int64(mo.Price),
		ClientID: int64(mo.ClientID),
		PvzID:    int64(mo.PvzID),
	}, nil
}

func (ret *ReturnHandler) GetMonOrder(ctx context.Context, ID int) (*MonOrder, error) {

	var (
		mo  repo.Monitoring
		err error
	)

	if mo, err = ret.R.Cache.Get(ctx, ID); errors.Is(err, redis.Nil) {
		mo, err = ret.R.Repo.GetOrder(ctx, ID)
		if errors.Is(err, repo.ErrNotFound) {
			return nil, status.Error(codes.NotFound, err.Error())
		}

		if err = ret.R.Cache.Set(ctx, mo); err != nil {
			return nil, err
		}
	}

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &MonOrder{
		ID:       mo.ID,
		State:    mo.State,
		Price:    mo.Price,
		ClientID: mo.ClientID,
		PvzID:    mo.PvzID,
	}, nil
}
