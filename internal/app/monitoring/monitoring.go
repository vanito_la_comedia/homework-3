package monitoring

import (
	"context"
	"fmt"
	"github.com/Shopify/sarama"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"gitlab.ozon.dev/vanito_la_comedia/homework-3/config"
	"gitlab.ozon.dev/vanito_la_comedia/homework-3/internal/cache"
	repo "gitlab.ozon.dev/vanito_la_comedia/homework-3/internal/repository/monitoring"
	pb "gitlab.ozon.dev/vanito_la_comedia/homework-3/pkg/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"net"
	"net/http"
)

type monitoring struct {
	returnConsumer *ReturnHandler
}

type monRepo struct {
	Repo  repo.MonRepository
	Cache cache.Cache
}

func NewRepo(Repo repo.MonRepository, Cache cache.Cache) *monRepo {
	return &monRepo{
		Repo:  Repo,
		Cache: Cache,
	}
}

func New(ctx context.Context, cfg *config.Config, repository *monRepo) (*monitoring, error) {

	scfg := sarama.NewConfig()

	retConsumer, err := initReturnConsumer(ctx, cfg, scfg, repository)
	if err != nil {
		log.Printf("failed init return consumer :%v", err)
		return nil, err
	}

	monURL := fmt.Sprintf("%s:%d", cfg.Monitoring.Host, cfg.Monitoring.Port)
	gatewayURL := fmt.Sprintf("%s:%d", cfg.Monitoring.Gwhost, cfg.Monitoring.Gwport)
	gatewayServer := NewGatewayServer(ctx, monURL)

	go func() {
		log.Println("Gateway server start on ", gatewayURL)
		if err := http.ListenAndServe(gatewayURL, gatewayServer); err != nil {
			log.Fatal("Failed start gateway server")
		}
	}()

	lis, err := net.Listen("tcp", monURL)

	if err != nil {
		return nil, fmt.Errorf("failed to listen: %v", err)
	}

	grpcServer := grpc.NewServer()
	pb.RegisterMonitoringServer(grpcServer, retConsumer)
	go func() {
		err = grpcServer.Serve(lis)
		if err != nil {
			log.Fatalf("failed to serve grpc server: %v", err)
		}
	}()

	return &monitoring{
		returnConsumer: retConsumer,
	}, nil
}

func NewGatewayServer(ctx context.Context, URL string) *runtime.ServeMux {

	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}
	err := pb.RegisterMonitoringHandlerFromEndpoint(ctx, mux, URL, opts)
	if err != nil {
		log.Fatal("can't register gateway server")
	}

	return mux
}
