package pvz

import (
	"context"
	"fmt"
	"github.com/Shopify/sarama"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.ozon.dev/vanito_la_comedia/homework-3/config"
	"log"
	"net/http"

	repo "gitlab.ozon.dev/vanito_la_comedia/homework-3/internal/repository/pvz"
)

type pvz struct {
	returnConsumer   *ReturnHandler
	rollbackConsumer *RollbackHandler
}

type pvzRepo struct {
	Repo repo.PVZRepository
}

func NewRepo(Repo repo.PVZRepository) *pvzRepo {
	return &pvzRepo{Repo: Repo}
}

func New(ctx context.Context, cfg *config.Config, repository *pvzRepo) (*pvz, error) {

	scfg := sarama.NewConfig()
	scfg.Producer.Return.Successes = true

	retConsumer, err := initReturnConsumer(ctx, cfg, scfg, repository)
	if err != nil {
		log.Printf("failed init return consumer : %v", err)
		return nil, err
	}

	rolConsumer, err := initRollbackConsumer(ctx, cfg, scfg, repository)
	if err != nil {
		log.Printf("failed init rollback consumer :%v", err)
		return nil, err
	}

	addr := fmt.Sprintf("%s:%d", cfg.Pvz.Host, cfg.Pvz.Port)

	http.Handle("/metrics", promhttp.Handler())
	go func() {
		log.Printf("Starting web server at %s\n", addr)
		err := http.ListenAndServe(addr, nil)
		if err != nil {
			log.Printf("failed start server :%v", err)
		}
	}()

	return &pvz{
		returnConsumer:   retConsumer,
		rollbackConsumer: rolConsumer,
	}, nil
}
