package redis

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis/v8"
	_ "github.com/go-redis/redis/v8"
	repository "gitlab.ozon.dev/vanito_la_comedia/homework-3/internal/repository/monitoring"
	"time"
)

type cache struct {
	client *redis.Client
}

func New() *cache {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "",
		DB:       0,
	})
	return &cache{rdb}
}

func (c *cache) Get(ctx context.Context, ID int) (repository.Monitoring, error) {

	var mon repository.Monitoring

	it, err := c.client.Get(ctx, fmt.Sprint(ID)).Result()
	if err != nil {
		return mon, err
	}

	if err = json.Unmarshal([]byte(it), &mon); err != nil {
		return mon, fmt.Errorf("can't unmarshal redis item %v", err)
	}

	return mon, nil
}

func (c *cache) Set(ctx context.Context, mon repository.Monitoring) (err error) {

	data, err := json.Marshal(mon)
	if err != nil {
		return
	}

	return c.client.Set(ctx, fmt.Sprint(mon.ID), data, time.Minute*5).Err()
}
