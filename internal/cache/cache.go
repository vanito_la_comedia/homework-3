package cache

import (
	"context"
	repository "gitlab.ozon.dev/vanito_la_comedia/homework-3/internal/repository/monitoring"
)

type Cache interface {
	Get(context.Context, int) (repository.Monitoring, error)
	Set(context.Context, repository.Monitoring) error
}
