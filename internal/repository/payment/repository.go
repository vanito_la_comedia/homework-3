package repository

import (
	"context"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

var ErrNotFound = errors.New("not found")

type Payment struct {
	ID    int    `json:"id"`
	State string `json:"state"`
	Price int    `json:"price"`
}

type PayRepository interface {
	AddPayment(context.Context, Payment) error
	DelPayment(context.Context, Payment) error
	ReturnPayment(context.Context, Payment) error
	PayPayment(context.Context, Payment) error
}

type payRepository struct {
	pool *pgxpool.Pool
}

func NewPayment(pool *pgxpool.Pool) *payRepository {
	return &payRepository{pool: pool}
}

func (pay *payRepository) AddPayment(ctx context.Context, p Payment) error {

	const query = `
		insert into payments (
			id,
			state,
			price
		) VALUES (
			$1, $2, $3
		)
	`
	var ID int64
	err := pay.pool.QueryRow(ctx, query,
		p.ID,
		"Ожидает оплаты",
		p.Price,
	).Scan(&ID)

	if err != nil {
		return fmt.Errorf("database: failed add payment %d:  %v", p.ID, err)
	}

	return nil
}

func (pay *payRepository) DelPayment(ctx context.Context, p Payment) error {
	const query = `
		delete from payments
		where id = $1;
	`

	cmd, err := pay.pool.Exec(ctx, query, p.ID)
	if err != nil {
		return fmt.Errorf("database: failed del payment %v", err)
	}
	if cmd.RowsAffected() == 0 {
		err = ErrNotFound
		return err
	}
	return nil
}

func (pay *payRepository) ReturnPayment(ctx context.Context, p Payment) error {
	const query = `
		update payments
		set state=$1 
		where id = $2
		returning id
	`
	var ID int64
	err := pay.pool.QueryRow(ctx, query,
		"Платеж отменен",
		p.ID,
	).Scan(&ID)
	if errors.Is(err, pgx.ErrNoRows) {
		ID = 0
		err = ErrNotFound
		return err
	}
	if err != nil {
		return fmt.Errorf("database: failed return payment %d %v", p.ID, err)
	}

	return nil
}

func (pay *payRepository) PayPayment(ctx context.Context, p Payment) error {
	const query = `
		update payments
		set state=$1 
		where id = $2
		returning id
	`
	var ID int64
	err := pay.pool.QueryRow(ctx, query,
		"Заказ оплачен",
		p.ID,
	).Scan(&ID)
	if errors.Is(err, pgx.ErrNoRows) {
		ID = 0
		err = ErrNotFound
		return err
	}
	if err != nil {
		return fmt.Errorf("database: failed pay order %d %v", p.ID, err)
	}

	return nil
}
