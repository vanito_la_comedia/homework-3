package repository

import (
	"context"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

var ErrNotFound = errors.New("not found")

type Order struct {
	ID    int    `json:"id"`
	State string `json:"state"`
}

type PVZRepository interface {
	AddOrder(context.Context, Order) error
	DelOrder(context.Context, Order) error
	ReturnOrder(context.Context, Order) error
	GiveOrder(context.Context, Order) error
}

type pvzRepository struct {
	pool *pgxpool.Pool
}

func NewPVZ(pool *pgxpool.Pool) *pvzRepository {
	return &pvzRepository{pool: pool}
}

func (pvz *pvzRepository) AddOrder(ctx context.Context, o Order) error {

	const query = `
		insert into orders (
			id,
			state
		) VALUES (
			$1, $2
		)
	`
	var ID int64
	err := pvz.pool.QueryRow(ctx, query,
		o.ID,
		"Ожидает выдачи в ПВЗ",
	).Scan(&ID)

	if err != nil {
		return fmt.Errorf("database: failed add order %d:  %v", o.ID, err)
	}

	return nil
}

func (pvz *pvzRepository) DelOrder(ctx context.Context, o Order) error {
	const query = `
		delete from orders
		where id = $1;
	`

	cmd, err := pvz.pool.Exec(ctx, query, o.ID)
	if err != nil {
		return fmt.Errorf("database: failed del order %v", err)
	}
	if cmd.RowsAffected() == 0 {
		err = ErrNotFound
		return err
	}
	return nil
}

func (pvz *pvzRepository) ReturnOrder(ctx context.Context, o Order) error {
	const query = `
		update orders
		set state=$1 
		where id = $2
		returning id
	`
	var ID int64
	err := pvz.pool.QueryRow(ctx, query,
		"Заказ отменен",
		o.ID,
	).Scan(&ID)
	if errors.Is(err, pgx.ErrNoRows) {
		ID = 0
		err = ErrNotFound
		return err
	}
	if err != nil {
		return fmt.Errorf("database: failed return order %d %v", o.ID, err)
	}

	return nil
}

func (pvz *pvzRepository) GiveOrder(ctx context.Context, o Order) error {
	const query = `
		update orders
		set state=$1 
		where id = $2
		returning id
	`
	var ID int64
	err := pvz.pool.QueryRow(ctx, query,
		"Выдан клиенту",
		o.ID,
	).Scan(&ID)
	if errors.Is(err, pgx.ErrNoRows) {
		ID = 0
		err = ErrNotFound
		return err
	}
	if err != nil {
		return fmt.Errorf("database: failed give order %d %v", o.ID, err)
	}

	return nil
}
