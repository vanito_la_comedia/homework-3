package repository

import (
	"context"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

var ErrNotFound = errors.New("not found")

type Monitoring struct {
	ID       int    `json:"id"`
	State    string `json:"state"`
	Price    int    `json:"price"`
	ClientID int    `json:"clientid"`
	PvzID    int    `json:"pvzid"`
}

type MonRepository interface {
	AddOrder(context.Context, Monitoring) error
	DelOrder(context.Context, Monitoring) error
	UpdateOrder(context.Context, Monitoring) error
	GetOrder(context.Context, int) (Monitoring, error)
}

type monRepository struct {
	pool *pgxpool.Pool
}

func NewMonitoring(pool *pgxpool.Pool) *monRepository {
	return &monRepository{pool: pool}
}

func (mon *monRepository) AddOrder(ctx context.Context, m Monitoring) error {

	const query = `
		insert into monitoring (
			id,
			state,
			price,
			clientid,
			pvzid
		) VALUES (
			$1, $2, $3, $4, $5
		)
	`
	var ID int64
	err := mon.pool.QueryRow(ctx, query,
		m.ID,
		"Ожидает оплаты",
		m.Price,
		m.ClientID,
		m.PvzID,
	).Scan(&ID)

	if err != nil {
		return fmt.Errorf("database: failed add order for monitoring %d:  %v", m.ID, err)
	}

	return nil
}

func (mon *monRepository) DelOrder(ctx context.Context, m Monitoring) error {
	const query = `
		delete from monitoring
		where id = $1;
	`

	cmd, err := mon.pool.Exec(ctx, query, m.ID)
	if err != nil {
		return fmt.Errorf("database: failed del order %v", err)
	}
	if cmd.RowsAffected() == 0 {
		err = ErrNotFound
		return err
	}
	return nil
}

func (mon *monRepository) UpdateOrder(ctx context.Context, m Monitoring) error {
	const query = `
		update monitoring
		set state=$1 
		where id = $2
		returning id
	`
	var ID int64
	err := mon.pool.QueryRow(ctx, query,
		m.State,
		m.ID,
	).Scan(&ID)
	if errors.Is(err, pgx.ErrNoRows) {
		ID = 0
		err = ErrNotFound
		return err
	}
	if err != nil {
		return fmt.Errorf("database: failed update order %d %v", m.ID, err)
	}

	return nil
}

func (mon *monRepository) GetOrder(ctx context.Context, ID int) (Monitoring, error) {
	const query = `
		select id,
			state,
			price,
			clientid,
			pvzid
		from monitoring
		where id = $1;
	`
	var o Monitoring
	err := mon.pool.QueryRow(ctx, query, ID).Scan(
		&o.ID,
		&o.State,
		&o.Price,
		&o.ClientID,
		&o.PvzID,
	)
	if errors.Is(err, pgx.ErrNoRows) {
		err = ErrNotFound
		return o, err
	}
	if err != nil {
		return o, fmt.Errorf("database: failed get order %d %v", o.ID, err)
	}

	return o, nil
}
