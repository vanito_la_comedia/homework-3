create extension postgres_fdw;

create table orders (id integer, state text NOT NULL) partition by hash (id);
create server shard1 foreign data wrapper postgres_fdw options (host 'shard1', port '5432', dbname 'postgres');
create server shard2 foreign data wrapper postgres_fdw options (host 'shard2', port '5432', dbname 'postgres');

create foreign table orders_1 partition of orders for values with (modulus 2, remainder 0) server shard1;
create foreign table orders_2 partition of orders for values with (modulus 2, remainder 1) server shard2;

create user mapping for postgres server shard1 options (user 'postgres', password 'postgres');
create user mapping for postgres server shard2 options (user 'postgres', password 'postgres');

insert into orders (id, state) VALUES ( 1000, 'Выдан');
insert into orders (id, state) VALUES ( 1001, 'Выдан');
insert into orders (id, state) VALUES ( 1002, 'Выдан');
insert into orders (id, state) VALUES ( 1003, 'Выдан');
insert into orders (id, state) VALUES ( 1004, 'Выдан');
insert into orders (id, state) VALUES ( 1005, 'Выдан');
insert into orders (id, state) VALUES ( 1006, 'Выдан');
insert into orders (id, state) VALUES ( 1007, 'Выдан');
insert into orders (id, state) VALUES ( 1008, 'Выдан');
insert into orders (id, state) VALUES ( 1009, 'Выдан');
insert into orders (id, state) VALUES ( 1010, 'Выдан');
insert into orders (id, state) VALUES ( 1011, 'Выдан');


create table payments (id integer, state text NOT NULL, price integer NOT NULL) partition by hash (id);
create foreign table payments_1 partition of payments for values with (modulus 2, remainder 0) server shard1;
create foreign table payments_2 partition of payments for values with (modulus 2, remainder 1) server shard2;

insert into payments (id, state, price) VALUES ( 1000, 'Оплачен', 500);
insert into payments (id, state, price) VALUES ( 1001, 'Оплачен', 300);
insert into payments (id, state, price) VALUES ( 1002, 'Оплачен', 200);
insert into payments (id, state, price) VALUES ( 1003, 'Оплачен', 100500);
insert into payments (id, state, price) VALUES ( 1004, 'Оплачен', 550);
insert into payments (id, state, price) VALUES ( 1005, 'Оплачен', 100);
insert into payments (id, state, price) VALUES ( 1006, 'Оплачен', 500);
insert into payments (id, state, price) VALUES ( 1007, 'Оплачен', 300);
insert into payments (id, state, price) VALUES ( 1008, 'Оплачен', 200);
insert into payments (id, state, price) VALUES ( 1009, 'Оплачен', 200500);
insert into payments (id, state, price) VALUES ( 1010, 'Оплачен', 530);
insert into payments (id, state, price) VALUES ( 1011, 'Оплачен', 70);


create table monitoring (id integer, state text NOT NULL, price integer NOT NULL,
 clientid integer NOT NULL, pvzid integer  NOT NULL) partition by hash (id);
create foreign table monitoring_1 partition of monitoring for values with (modulus 2, remainder 0) server shard1;
create foreign table monitoring_2 partition of monitoring for values with (modulus 2, remainder 1) server shard2;

insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1000, 'Оплачен', 500, 100, 1);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1001, 'Оплачен', 300, 101, 1);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1002, 'Оплачен', 200, 102, 1);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1003, 'Оплачен', 100500, 103, 1);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1004, 'Оплачен', 550, 104, 2);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1005, 'Оплачен', 100, 105, 2);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1006, 'Оплачен', 500, 106, 2);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1007, 'Оплачен', 300, 107, 2);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1008, 'Оплачен', 200, 108, 3);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1009, 'Оплачен', 200500, 109, 3);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1010, 'Оплачен', 530, 110, 3);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1011, 'Оплачен', 70, 111, 3);