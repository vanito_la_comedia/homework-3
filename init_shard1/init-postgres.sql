CREATE TABLE IF NOT EXISTS orders_1 (
    id          integer          PRIMARY KEY,
    state       text     NOT NULL
);

CREATE TABLE IF NOT EXISTS payments_1 (
    id          integer          PRIMARY KEY,
    state       text     NOT NULL,
    price       integer  NOT NULL
);

CREATE TABLE IF NOT EXISTS monitoring_1 (
    id          integer  PRIMARY KEY,
    state       text     NOT NULL,
    price       integer  NOT NULL,
    clientid    integer  NOT NULL,
    pvzid       integer  NOT NULL
);