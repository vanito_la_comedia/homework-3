#!/bin/bash
psql -d postgres <<-EOSQL
CREATE TABLE IF NOT EXISTS orders (
    id          integer          PRIMARY KEY,
    state       text     NOT NULL
);
insert into orders (id, state) VALUES ( 1000, 'Выдан');
insert into orders (id, state) VALUES ( 1001, 'Выдан');
insert into orders (id, state) VALUES ( 1002, 'Выдан');
insert into orders (id, state) VALUES ( 1003, 'Выдан');
insert into orders (id, state) VALUES ( 1004, 'Выдан');
insert into orders (id, state) VALUES ( 1005, 'Выдан');
insert into orders (id, state) VALUES ( 1006, 'Выдан');
insert into orders (id, state) VALUES ( 1007, 'Выдан');
insert into orders (id, state) VALUES ( 1008, 'Выдан');
insert into orders (id, state) VALUES ( 1009, 'Выдан');
insert into orders (id, state) VALUES ( 1010, 'Выдан');
insert into orders (id, state) VALUES ( 1011, 'Выдан');

CREATE TABLE IF NOT EXISTS payments (
    id          integer          PRIMARY KEY,
    state       text     NOT NULL,
    price       integer  NOT NULL
);
insert into payments (id, state, price) VALUES ( 1000, 'Оплачен', 500);
insert into payments (id, state, price) VALUES ( 1001, 'Оплачен', 300);
insert into payments (id, state, price) VALUES ( 1002, 'Оплачен', 200);
insert into payments (id, state, price) VALUES ( 1003, 'Оплачен', 100500);
insert into payments (id, state, price) VALUES ( 1004, 'Оплачен', 550);
insert into payments (id, state, price) VALUES ( 1005, 'Оплачен', 100);
insert into payments (id, state, price) VALUES ( 1006, 'Оплачен', 500);
insert into payments (id, state, price) VALUES ( 1007, 'Оплачен', 300);
insert into payments (id, state, price) VALUES ( 1008, 'Оплачен', 200);
insert into payments (id, state, price) VALUES ( 1009, 'Оплачен', 200500);
insert into payments (id, state, price) VALUES ( 1010, 'Оплачен', 530);
insert into payments (id, state, price) VALUES ( 1011, 'Оплачен', 70);

CREATE TABLE IF NOT EXISTS monitoring (
    id          integer  PRIMARY KEY,
    state       text     NOT NULL,
    price       integer  NOT NULL,
    clientid    integer  NOT NULL,
    pvzid       integer  NOT NULL
);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1000, 'Оплачен', 500, 100, 1);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1001, 'Оплачен', 300, 101, 1);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1002, 'Оплачен', 200, 102, 1);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1003, 'Оплачен', 100500, 103, 1);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1004, 'Оплачен', 550, 104, 2);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1005, 'Оплачен', 100, 105, 2);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1006, 'Оплачен', 500, 106, 2);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1007, 'Оплачен', 300, 107, 2);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1008, 'Оплачен', 200, 108, 3);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1009, 'Оплачен', 200500, 109, 3);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1010, 'Оплачен', 530, 110, 3);
insert into monitoring (id, state, price, clientid, pvzid) VALUES ( 1011, 'Оплачен', 70, 111, 3);
EOSQL