package main

import (
	"context"
	"fmt"
	"gitlab.ozon.dev/vanito_la_comedia/homework-3/config"
	"gitlab.ozon.dev/vanito_la_comedia/homework-3/internal/app/pvz"
	"gitlab.ozon.dev/vanito_la_comedia/homework-3/internal/database"
	repo "gitlab.ozon.dev/vanito_la_comedia/homework-3/internal/repository/pvz"
	"log"
	"os"
)

func main() {
	b, err := os.ReadFile("./config/config.yaml")
	if err != nil {
		log.Fatal(err)
	}

	cfg, err := config.ParseConfig(b)
	if err != nil {
		log.Fatal(err)
	}

	dsn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s",
		cfg.Pvz.Dbhost, cfg.Pvz.Dbport, cfg.Pvz.Dbuser, cfg.Pvz.Dbpassword, cfg.Pvz.Dbname, cfg.Pvz.DbSslmode)
	log.Println("dsn=", dsn)

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	db, err := database.New(ctx, dsn)
	if err != nil {
		log.Fatalf("failed init pvz postgres :%v", err)
	}
	defer db.Close()

	repository := repo.NewPVZ(db)
	pvzRepo := pvz.NewRepo(repository)

	_, err = pvz.New(ctx, cfg, pvzRepo)
	if err != nil {
		log.Fatalf("NewPVZ: %v", err)
	}
	<-ctx.Done()
}
