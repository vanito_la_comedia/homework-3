package main

import (
	"context"
	"fmt"
	"gitlab.ozon.dev/vanito_la_comedia/homework-3/config"
	"gitlab.ozon.dev/vanito_la_comedia/homework-3/internal/app/payment"
	"gitlab.ozon.dev/vanito_la_comedia/homework-3/internal/database"
	repo "gitlab.ozon.dev/vanito_la_comedia/homework-3/internal/repository/payment"
	"log"
	"os"
)

func main() {
	b, err := os.ReadFile("./config/config.yaml")
	if err != nil {
		log.Fatal(err)
	}

	cfg, err := config.ParseConfig(b)
	if err != nil {
		log.Fatal(err)
	}

	dsn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s",
		cfg.Payment.Dbhost, cfg.Payment.Dbport, cfg.Payment.Dbuser, cfg.Payment.Dbpassword, cfg.Payment.Dbname, cfg.Payment.DbSslmode)
	log.Println("dsn=", dsn)

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	db, err := database.New(ctx, dsn)
	if err != nil {
		log.Fatalf("failed init pvz postgres :%v", err)
	}
	defer db.Close()

	repository := repo.NewPayment(db)
	paymentRepo := payment.NewRepo(repository)

	_, err = payment.New(ctx, cfg, paymentRepo)
	if err != nil {
		log.Fatalf("NewPayment: %v", err)
	}
	<-ctx.Done()
}
