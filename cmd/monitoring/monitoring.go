package main

import (
	"context"
	"fmt"
	"gitlab.ozon.dev/vanito_la_comedia/homework-3/config"
	"gitlab.ozon.dev/vanito_la_comedia/homework-3/internal/app/monitoring"
	"gitlab.ozon.dev/vanito_la_comedia/homework-3/internal/database"
	"gitlab.ozon.dev/vanito_la_comedia/homework-3/internal/redis"
	repo "gitlab.ozon.dev/vanito_la_comedia/homework-3/internal/repository/monitoring"
	"log"
	"os"
)

func main() {
	b, err := os.ReadFile("./config/config.yaml")
	if err != nil {
		log.Fatal(err)
	}

	cfg, err := config.ParseConfig(b)
	if err != nil {
		log.Fatal(err)
	}

	dsn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s",
		cfg.Monitoring.Dbhost, cfg.Monitoring.Dbport, cfg.Monitoring.Dbuser, cfg.Monitoring.Dbpassword, cfg.Monitoring.Dbname, cfg.Monitoring.DbSslmode)
	log.Println("dsn=", dsn)

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	db, err := database.New(ctx, dsn)
	if err != nil {
		log.Fatalf("failed init pvz postgres :%v", err)
	}
	defer db.Close()

	repository := repo.NewMonitoring(db)
	monitoringRepo := monitoring.NewRepo(repository, redis.New())

	_, err = monitoring.New(ctx, cfg, monitoringRepo)
	if err != nil {
		log.Fatalf("NewMonitoring: %v", err)
	}
	<-ctx.Done()
}
