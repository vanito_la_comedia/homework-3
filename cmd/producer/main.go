package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/Shopify/sarama"
	"log"
)

type Order struct {
	ID    int    `json:"id"`
	State string `json:"state"`
}

// Just for testing
func main() {

	id := flag.Int("id", 1000, "Enter order ID")
	flag.Parse()
	cfg := sarama.NewConfig()
	cfg.Producer.Return.Successes = true
	syncProducer, err := sarama.NewSyncProducer([]string{"localhost:9095", "localhost:9096"}, cfg)
	if err != nil {
		log.Fatalf("sync kafka: %v", err)
	}

	d := Order{
		ID:    *id,
		State: "Выдан",
	}
	b, err := json.Marshal(d)
	if err != nil {
		log.Printf("json marshal error: %v", err)
		//continue
	}
	par, off, err := syncProducer.SendMessage(&sarama.ProducerMessage{
		Topic: "return_orders",
		Key:   sarama.StringEncoder(fmt.Sprintf("%v", d.ID)),
		Value: sarama.ByteEncoder(b),
	})
	log.Printf("order %v -> %v; %v", par, off, err)
}
